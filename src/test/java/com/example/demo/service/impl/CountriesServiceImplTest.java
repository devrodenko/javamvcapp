package com.example.demo.service.impl;

import com.example.demo.controller.CountryParams;
import com.example.demo.dao.CountriesRepository;
import com.example.demo.dao.model.CountryEntity;
import com.example.demo.dto.CountryDto;
import lombok.SneakyThrows;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.MockitoAnnotations.openMocks;

class CountriesServiceImplTest {
    @Mock
    private CountriesRepository repository;

    @InjectMocks
    private CountriesServiceImpl service;

    @BeforeEach
    @SneakyThrows
    void setUp() {
        openMocks(this).close();
    }

    @Test
    void getCountriesByParamTest() {
        // Given
        Long id = 11L;
        String name = "TEST_NAME";
        CountryEntity result = CountryEntity.builder()
                .id(id)
                .name(name)
                .build();

        when(repository.findByName(name)).thenReturn(Optional.of(result));

        val actual = service.getCountriesByParam(CountryParams.builder().name(name).build());

        assertNotNull(actual);
        verify(repository).findByName(name);
    }

    @Test
    void createCountryTest() {
        // Given
        String name = "TEST_NAME";
        CountryDto request = CountryDto.builder()
                .name(name)
                .build();

        CountryEntity requestEntity = CountryEntity.builder()
                .name(request.getName())
                .build();

        Long id = 11L;
        CountryEntity entity = CountryEntity.builder()
                .id(id)
                .name(request.getName())
                .build();

        // When
        when(repository.save(requestEntity)).thenReturn(entity);
        CountryDto result = service.createCountry(request);

        // Then
        verify(repository).save(requestEntity);
        assertNotNull(result);
        assertEquals(id, result.getId());
    }
}