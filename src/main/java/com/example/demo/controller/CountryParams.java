package com.example.demo.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = PRIVATE)
public class CountryParams {
    Long id;
    String name;
    String text;
    Integer offset;
    Integer limit;

    public Integer getOffset() {
        return Math.max(0, offset != null ? offset : 0);
    }

    public Integer getLimit() {
        return Math.max(1, limit != null ? limit : 10);
    }
}
